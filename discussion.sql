-- Add five artists

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


-- Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES (

	"Fearless",
	"2008-1-1",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (

	"Red",
	"2012-1-1",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Fearless",
	246,
	"Pop rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Love Story",
	213,
	"Country pop",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"State of Grace",
	253,
	"Rock, alternative rock, arena rock",
	5

);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Red",
	204,
	"Country",
	5

);


-- Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES (

	"A Star is Born",
	"2018-1-1",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (

	"Born This Way",
	"2011-1-1",
	5
);


INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Black Eyes",
	151,
	"Rock",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Shallow",
	201,
	"Country, rock, folk rock",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Born This Way",
	252,
	"Electropop",
	7
);



-- Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES (

	"Purpose",
	"2015-1-1",
	6
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (

	"Believe",
	"2012-1-1",
	6
);


INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Sorry",
	132,
	"Dancehall",
	9
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (

	"Boyfriend",
	251,
	"Pop",
	10
);


-- [SECTION] Advanced Selects

-- Exclude records
SELECT * FROM songs WHERE id != 11;


-- Greater than
SELECT * FROM songs WHERE id > 11;

-- Greater than or equal
SELECT * FROM songs WHERE id >= 11;

-- Less than
SELECT * FROM songs WHERE id < 5;

-- Less than or equal
SELECT * FROM songs WHERE id <= 5;



-- Get specific IDs (OR)
SELECT * FROM songs where genre = "Pop" OR id = 2;

-- Get specific IDs (IN)
SELECT * FROM songs where genre IN ("Pop", "Rock");


-- Combining conditions
SELECT * FROM songs WHERE album_id = 2 AND id < 5;



-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%y"; -- partial match with the last character(s)

SELECT * FROM songs WHERE song_name LIKE "s%"; -- partial match with the starting character(s)

SELECT * FROM songs WHERE song_name LIKE "%a%"; -- partial match from the middle character(s)



-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;

SELECT * FROM songs ORDER BY song_name DESC;

SELECT * FROM songs ORDER BY RAND();


-- Limiting record output
SELECT * FROM songs LIMIT 3;


--  Getting distinct records
SELECT DISTINCT genre FROM songs;





















